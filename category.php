<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package regiment-hero
 */

get_header();?>


		<div id="page" class="hfeed site">
			<div id="header-bump"></div>
			<!--  https://blogsneeraj.wordpress.com/2016/02/16/acf-get-custom-field-value-from-category/ -->
			<?php
			global $wp_query; // get the query object
			$cat_obj = $wp_query->get_queried_object();
			?>
			<!-- setting the condition -->
			<?php if ( get_field('category_hero_image',$cat_obj->taxonomy.'_'.$cat_obj->term_id ) ): ?>
			<div id="hero-top">		
			<img src="<?php echo get_field('category_hero_image',$cat_obj->taxonomy.'_'.$cat_obj->term_id); ?>"/>

			<h1><?php single_cat_title();?></h1>
			</div>
			<?php endif; ?>	

			<?php if ( !get_field('category_hero_image',$cat_obj->taxonomy.'_'.$cat_obj->term_id ) ): ?>
				<div id="big-header-bump"></div>
			<?php endif; ?>	
				
	<div id="content" class="site-content" >

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
<header class="page-header">
				<!--  https://blogsneeraj.wordpress.com/2016/02/16/acf-get-custom-field-value-from-category/ -->	
				<?php
				global $wp_query; // get the query object
				$cat_obj = $wp_query->get_queried_object();
				?>
				<!-- setting the condition -->
				<?php if ( !get_field('category_hero_image',$cat_obj->taxonomy.'_'.$cat_obj->term_id ) ): ?>
				<h1><?php single_cat_title();?></h1>
				<?php endif; ?>

				<?php
				// Show an optional term description.
				$term_description = term_description();
				if ( ! empty( $term_description ) ) :
				printf( '<div class="taxonomy-description">%s</div>', $term_description );?>
				<hr/>	
			<?php endif;?>
</header><!-- .page-header -->			
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<ul class="just-archive-array">
			<?php global $query_string;
query_posts( $query_string.'&order=DESC' );	?>
			<?php while ( have_posts() ) : the_post(); ?>
<!-- POST FIELDS-->
<li>	
<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
<div class="entry-content">  
	<div class="archive_left_picture">	
	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
	</div><!-- ends left picture -->

<?php if (!empty($post->post_excerpt)) : ?>
		<div class="archive_right_text">
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->
<?php the_excerpt();?>

<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark">read more</a></p>

<?php else : ?>
		<div class="archive_right_text">
		<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>	
		</header><!-- .entry-header -->
<?php the_content(); ?>

<?php endif; ?>
		</div><!-- ends right text -->
	</div>	<!-- ends entry content -->
</article><!-- #post-## -->
<div class="clear"></div>

<?php   } else { ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  

	<div class="entry-content">
		<header class="entry-header">
		
		</header><!-- .entry-header -->
<?php if (!empty($post->post_excerpt)) : ?>
<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
<?php the_excerpt();?>

<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark">read more</a></p>

<?php else : ?>
<h1 class="entry-title"><?php the_title(); ?></h1>	
<?php the_content(); ?>

<?php endif; ?>
	</div><!-- .entry-content -->
 <?php    } ?>

</article><!-- #post-## -->
<div class="clear"><hr/></div>
</li>
	<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>	
	<?php endif; ?>
<!-- ENDS POST FIELDS -->
			<?php endwhile; ?>
</ul><!-- ends archive array-->
			<?php regiment_hero_paging_nav(); ?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
		</main><!-- #main -->
	</section><!-- #primary -->
	<div id="secondary" class="widget-area" role="complementary">		
<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>										
					<?php endif; // end sidebar widget area ?>	
	</div><!-- #secondary -->

<?php get_footer(); ?>
