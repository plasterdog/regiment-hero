<?php
/*
*Template Name: Home Page
 * @package regiment-hero
 */


get_header(); ?>
<div id="header-bump"></div>


		

			<?php if ( get_field( 'page_hero_image' ) ): ?>
<div id="home-hero-top">		
<img src="<?php echo esc_url( get_field( 'page_hero_image' ) ); ?>"/>	
<div class="home-hero-content">
<h1><?php the_field('home_hero_title'); ?></h1>
<h2><?php the_field('home_hero_sub_title'); ?></h2>
<p> <?php the_field('home_hero_text_excerpt'); ?></p>
<p> <?php the_field('home_hero_second_text_line'); ?></p>
<p> <?php the_field('home_hero_third_text_line'); ?></p>
<p> <?php the_field('home_hero_fourth_text_line'); ?></p>
<h5 class="top-cta-button"><a href="<?php the_field('home_hero_call_to_action_target'); ?>"> <?php the_field('home_hero_call_to_action'); ?></a>
</div><!-- ends hero content -->

</div><!-- ends hero top -->
<?php endif; ?>	
<div id="page" class="hfeed site">
<?php if (! get_field( 'page_hero_image' ) ): ?>
<div id="big-header-bump"></div>
<?php endif; ?>	
	<div id="content" class="site-content" >
	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'regiment-hero' ),
				'after'  => '</div>',
			) );
		?>
<div class="clear">
<div class="left-side cta-section">
	<?php if(get_field('home_left_button_title')) {?>
			<h3><?php the_field('home_left_button_title'); ?><h3>
			<h4><?php the_field('home_left_button_sub_title'); ?><h4>
			<h5 class="cta-button"><a href="<?php the_field('home_left_button_taget'); ?>"> <?php the_field('home_left_button_label'); ?></a>
	<?php } ?><!-- ends the first condition -->
	<?php if(!get_field('home_left_button_title')) {?>

	<?php }?> <!-- ends the second outer condition -->  
</div><!-- ends left side-->
<div class="right-side cta-section">
	<?php if(get_field('home_right_button_title')) {?>
			<h3><?php the_field('home_right_button_title'); ?><h3>
			<h4><?php the_field('home_right_button_sub_title'); ?><h4>
			<h5 class="cta-button"><a href="<?php the_field('home_right_button_taget'); ?>"> <?php the_field('home_right_button_label'); ?></a>
	<?php } ?><!-- ends the first condition -->
	<?php if(!get_field('home_left_button_title')) {?>

	<?php }?> <!-- ends the second outer condition -->  	
</div><!-- ends right side-->
</div><!-- ends left / right container -->		
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'regiment-hero' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	
	<div class="clear" style="height:2em;"></div>

<?php get_footer(); ?>
