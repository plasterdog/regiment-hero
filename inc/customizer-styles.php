<?php
//JMC => https://code.tutsplus.com/tutorials/a-guide-to-the-wordpress-theme-customizer-adding-a-new-setting--wp-33180
//CUSTOMIZER COLOR ADDITIONS

//SETTING UP THE NEW COLOR CONTROL: LINKS
function pdog14_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_overlay_color',
        array(
            'default'     => '#008cc1',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'overlay_color',
            array(
                'label'      => __( 'Overlay Color', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_overlay_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog14_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog14_customizer_css() {
    ?>
    <style type="text/css">
      .right-overlay{background-color: <?php echo get_theme_mod( 'pdog_overlay_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog14_customizer_css' );




//SETTING UP THE NEW COLOR CONTROL: LINKS
function pdog1_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_link_color',
        array(
            'default'     => '#00649c',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_color',
            array(
                'label'      => __( 'Link Color', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_link_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog1_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog1_customizer_css() {
    ?>
    <style type="text/css">
       a { color: <?php echo get_theme_mod( 'pdog_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog1_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: LINKS:HOVER
function pdog2_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_link_hover',
        array(
            'default'     => '#001f30',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_hover',
            array(
                'label'      => __( 'Link Hover', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_link_hover'
            )
        )
    );
}
add_action( 'customize_register', 'pdog2_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog2_customizer_css() {
    ?>
    <style type="text/css">
       a:hover { color: <?php echo get_theme_mod( 'pdog_link_hover' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog2_customizer_css' );
 //SETTING UP THE NEW COLOR CONTROL: NAVIGATION LINKS
function pdog1a_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_nav_link_color',
        array(
            'default'     => '#4e4f51',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'nav_link_color',
            array(
                'label'      => __( 'Nav Link Color', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_nav_link_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog1a_register_theme_customizer' );

//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog1a_customizer_css() {
    ?>
    <style type="text/css">
     .main-navigation  a { color: <?php echo get_theme_mod( 'pdog_nav_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog1a_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: NAV LINKS:HOVER
function pdog2a_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_nav_link_hover',
        array(
            'default'     => '#1f1f1f',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'nav_link_hover',
            array(
                'label'      => __( 'Nav Link Hover', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_nav_link_hover'
            )
        )
    );
}
add_action( 'customize_register', 'pdog2a_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog2a_customizer_css() {
    ?>
    <style type="text/css">
       .main-navigation li a:hover,  .main-navigation a:hover, .main-navigation li:hover a, .main-navigation li.current_page_item a {
    color: red;
}{ color: <?php echo get_theme_mod( 'pdog_nav_link_hover' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog2a_customizer_css' );


//SETTING UP THE NEW COLOR CONTROL: HEADINGS COLOR
function pdog5_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_headings_color',
        array(
            'default'     => '#008cc1',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'headings_color',
            array(
                'label'      => __( 'Headings Color', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_headings_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog5_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog5_customizer_css() {
    ?>
    <style type="text/css">
      h1, h2, h3, h4, h5, h6, h1.page-title, h1.entry-title,h1.page-title a, h1.entry-title a, h1.responsive-page-title, h1.responsive-page-title a, h1.answer-heading, .single-question h1.page-title, .inner-focus-item h3,.inner-focus-item h3 a, #hero-top h1 { color: <?php echo get_theme_mod( 'pdog_headings_color' ); ?>; }
      button, input[type="button"], input[type="reset"], input[type="submit"]{ background-color: <?php echo get_theme_mod( 'pdog_headings_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog5_customizer_css' );
//SETTING UP THE NEW COLOR CONTROL: BODY TEXT
function pdog6_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_body_text',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'body_text',
            array(
                'label'      => __( 'Body Text Color', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_body_text'
            )
        )
    );
}
add_action( 'customize_register', 'pdog6_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog6_customizer_css() {
    ?>
    <style type="text/css">
       body { color: <?php echo get_theme_mod( 'pdog_body_text' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog6_customizer_css' );


//SETTING UP THE NEW COLOR CONTROL: HEADER BACKGROUND COLOR
function pdog8_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_header_background',
        array(
            'default'     => '#ffffff',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'header_background',
            array(
                'label'      => __( 'Header Area Background', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_header_background'
            )
        )
    );
}
add_action( 'customize_register', 'pdog8_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog8_customizer_css() {
    ?>
    <style type="text/css">
      #full-top, .focus-group { background: <?php echo get_theme_mod( 'pdog_header_background' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog8_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: FOOTER BACKGROUND COLOR
function pdog9_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_foot_background',
        array(
            'default'     => '#ffffff',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'foot_background',
            array(
                'label'      => __( 'Footer Area Background', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_foot_background'
            )
        )
    );
}
add_action( 'customize_register', 'pdog9_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog9_customizer_css() {
    ?>
    <style type="text/css">
      #colophon.site-footer { background: <?php echo get_theme_mod( 'pdog_foot_background' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog9_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: FOOTER TEXT COLOR
function pdog10_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_foot_text',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'foot_text',
            array(
                'label'      => __( 'Footer Text Color', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_foot_text'
            )
        )
    );
}
add_action( 'customize_register', 'pdog10_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog10_customizer_css() {
    ?>
    <style type="text/css">
      #foot-constraint {color: <?php echo get_theme_mod( 'pdog_foot_text' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog10_customizer_css' );

 //SETTING UP THE NEW COLOR CONTROL: FOOTER NAVIGATION LINKS
function pdog12_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_foot_nav_link_color',
        array(
            'default'     => '#000000',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'foot_nav_link_color',
            array(
                'label'      => __( 'Footer Nav Link Color', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_foot_nav_link_color'
            )
        )
    );
}
add_action( 'customize_register', 'pdog12_register_theme_customizer' );

//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog12_customizer_css() {
    ?>
    <style type="text/css">
     #colophon.site-footer a, #colophon .foot-social-icons a { color: <?php echo get_theme_mod( 'pdog_foot_nav_link_color' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog12_customizer_css' );

//SETTING UP THE NEW COLOR CONTROL: FOOTER NAV LINKS:HOVER
function pdog13_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'pdog_foot_nav_link_hover',
        array(
            'default'     => '#1f1f1f',
            'sanitize_callback'  => 'esc_attr',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'foot_nav_link_hover',
            array(
                'label'      => __( 'Footer Nav Link Hover', 'regiment-hero' ),
                'section'    => 'colors',
                'settings'   => 'pdog_foot_nav_link_hover'
            )
        )
    );
}
add_action( 'customize_register', 'pdog13_register_theme_customizer' );
//JMC THIS APPLIES THE COLOR ABOVE AS A CSS OVER-RIDE
function pdog13_customizer_css() {
    ?>
    <style type="text/css">
    #colophon.site-footer a:hover, , #colophon .foot-social-icons a:hover{ color: <?php echo get_theme_mod( 'pdog_foot_nav_link_hover' ); ?>; }
    </style>
    <?php
}
add_action( 'wp_head', 'pdog13_customizer_css' );


